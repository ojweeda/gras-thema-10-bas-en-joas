require([
    "esri/Map",
    "esri/views/MapView",
    "esri/layers/GraphicsLayer",
    "esri/widgets/Sketch",
    "esri/request",
    "esri/layers/FeatureLayer",
    "esri/widgets/FeatureForm",
    "esri/widgets/BasemapGallery"
], function (Map, MapView, GraphicsLayer, Sketch, esriRequest, FeatureLayer, FeatureForm, BasemapGallery) {
    //*** ADD ***//
    var graphicsLayer = new GraphicsLayer();

    var map = new Map({
        basemap: "topo-vector",
        layers: [graphicsLayer]
    });

    var view = new MapView({
        container: "viewDiv",
        map: map,
        center: [6.609887, 53.166283], // longitude, latitude
        zoom: 12
    });
    var sketch = new Sketch({
        view: view,
        layer: graphicsLayer
    });

    view.ui.add(sketch, "top-right");

    var basemapGallery = new BasemapGallery({
        view: view
    });

    // view.ui.add(basemapGallery, "top-right");

    // listen to create event
    sketch.on("create", function (event) {
        // check if vertices are being added to the graphic that is being updated.
        const eventInfo = event.toolEventInfo;
        if (event.state === "complete") {
            const graphic = event.graphic;
            // Call function for entering data
            addEventData(graphic);
        }
    });

});

addEventData = function (graphic) {
    console.log(graphic);
};


