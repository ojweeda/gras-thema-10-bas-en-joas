require([
    "esri/Map",
    "esri/widgets/Editor",
    "esri/views/MapView",
    "esri/Graphic",
    "esri/layers/GraphicsLayer",
    "esri/geometry/support/webMercatorUtils"
], function(Map, Editor, MapView, Graphic, GraphicsLayer, webMercatorUtils) {

    var map = new Map({
        basemap: "topo-vector"
    });


    var view = new MapView({
        container: "viewDiv",
        map: map,
        center: [6.564446, 53.218038],
        zoom: 12
    });

    var graphicsLayer = new GraphicsLayer();
    map.add(graphicsLayer);

    fetch("/grassdata").then(function (response) {
        return response.json()
    }).then(function (data) {
        console.log(data);
        data.forEach(function(item) {
            let polygonArray = [];
            let polygonArrayxy = [];
            polygonString = item.area;
            coordinates = polygonString.split(',').map(c => { return parseFloat(c) });
            for (let i = 0; i < coordinates.length; i+=2) {
                polygonArray.push([coordinates[i] , coordinates[i+1]]);
            }
            for (let i = 0; i < polygonArray.length; i+=1) {
                polygonArrayxy.push(webMercatorUtils.xyToLngLat(polygonArray[i][0],polygonArray[i][1]));
            }

            var polygon = {
                type: "polygon",
                rings: polygonArrayxy
            };

            var simpleFillSymbol = {
                type: "simple-fill",
                color: [15, 166, 95, 0.5],  // orange, opacity 80%
                outline: {
                    color: [0, 0, 0],
                    width: 1
                }
            };

            var polygonGraphic = new Graphic({
                geometry: polygon,
                symbol: simpleFillSymbol,
            });

            polygonGraphic.attributes = {
                "date": item.date,
                "starttime": item.startTime,
                "endtime": item.endTime,
                "amount": item.amountOfGrass,
                "delivery": item.placeDelivery,
                "cost": item.cost
            };

            var template = {
                title: "Information",
                content: [
                    {
                        type: "fields",
                        fieldInfos: [
                            {
                                fieldName: "date",
                                label: "Date and time",
                            },
                            {
                                fieldName: "starttime",
                                label: "Start time"
                            },
                            {
                                fieldName: "endtime",
                                label: "End time"
                            },
                            {
                                fieldName: "delivery",
                                label: "Place of delivery"
                            },
                            {
                                fieldName: "cost",
                                label: "Costs of this place of mowed grass"
                            },
                            {
                                fieldName: "amount",
                                label: "Amount of grass mowed"
                            },
                        ]
                    }
                ],
            };

            polygonGraphic.popupTemplate = template;
            graphicsLayer.add(polygonGraphic);

        });
    });
});