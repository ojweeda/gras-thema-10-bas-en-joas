let geom = {};
//function for ArcGis map
$(function (name) {
    require([
        "esri/Map",
        "esri/widgets/Editor",
        "esri/views/MapView",
        "esri/layers/GraphicsLayer",
        "esri/widgets/Sketch",
        "esri/request",
        "esri/layers/FeatureLayer",
        "esri/widgets/FeatureForm",
        "esri/widgets/BasemapGallery"
    ], function (Map, Editor, MapView, GraphicsLayer, Sketch, esriRequest, FeatureLayer, FeatureForm, BasemapGallery) {

        // create graphics layer
        var graphicsLayer = new GraphicsLayer();

        // create map
        var map = new Map({
            basemap: "topo-vector",
            layers: [graphicsLayer]
        });

        // create map view
        var view = new MapView({
            container: "viewDiv",
            map: map,
            center: [6.564446, 53.218038], // longitude, latitude
            zoom: 11
        });

        // create sketch widget
        var sketch = new Sketch({
            view: view,
            layer: graphicsLayer
        });

        // add sketch widget to the topright corner
        view.ui.add(sketch, "top-right");

        // let editor = new Editor({
        //     view: view,
        //     layer: graphicsLayer
        // });

        // // add editor
        // view.ui.add(editor, "bottom-left");

        // listen to create event
        sketch.on("create", function (event) {
            // check if vertices are being added to the graphic that is being updated.
            if (event.state === "complete") {
                // view.ui.remove(sketch);
                const graphic = event.graphic;
                geom.rings = graphic.geometry.rings;
                addEventData(graphic)
                // Call function for entering data
            }
        });
    });

    // function for logging graphic data
    addEventData = function (graphic) {
        console.log(graphic);
    }

    /**
     * on click submit, form processing
     */
    $("#submit-all").click(function (event) {
        // Make sure the form doesn't actually submit
        event.preventDefault();

        // Get the selected file
        let formData = new FormData();
        let input = $("#form1").serializeArray();
        let emptyFields = [];

        // for each field in the form, add data to formData
        input.forEach(function (field) {
            if (field.value === "") {
                // if field is not filled in, add to emptyfields
                emptyFields.push(" " + field.name)
            } else {
                formData.append(field.name, field.value);
            }
            console.log("Adding" + field.name);
        });

        // if geom.rings is undefined, add to emptyfields
        if (geom.rings === undefined) {
            emptyFields.push(" location")
        } else {
            // add geom.rings to formData
            formData.append("coordinates", geom.rings);
        }

        console.log(emptyFields);

        // give alert of missing required fields
        if (emptyFields.length > 0) {
            alert("Please fill in the following fields: " + emptyFields);
        } else {
            // Get the plot data given the uploaded file
            fetch('/submit-page', {
                method: 'POST',
                body: formData,
            });

            // direct to overview map when form is filled in correctly
            window.location.href = '/overview.map';
        }
    });
});

