require([
    "esri/Map",
    "esri/widgets/Editor",
    "esri/views/MapView",
    "esri/Graphic",
    "esri/layers/GraphicsLayer",
    "esri/geometry/support/webMercatorUtils"
], function (Map, Editor, MapView, Graphic, GraphicsLayer, webMercatorUtils) {

    // create map
    var map = new Map({
        basemap: "topo-vector"
    });

    // create map view
    var view = new MapView({
        container: "viewDiv",
        map: map,
        center: [6.564446, 53.218038],
        zoom: 12
    });

    // create graphics layer
    var graphicsLayer = new GraphicsLayer();
    map.add(graphicsLayer);

    // gets coordinates from grassdata to fill the map with all the areas in the database
    fetch("/grassdata").then(function (response) {
        return response.json()
    }).then(function (data) {
        console.log(data);
        data.forEach(function (item) {
            let polygonArray = [];
            let polygonArrayxy = [];
            polygonString = item.area;
            // split the polygon string
            coordinates = polygonString.split(',').map(c => {
                return parseFloat(c)
            });
            for (let i = 0; i < coordinates.length; i += 2) {
                // push coordinates in empty array
                polygonArray.push([coordinates[i], coordinates[i + 1]]);
            }
            for (let i = 0; i < polygonArray.length; i += 1) {
                // convert xy coordinates to lat long coordinates
                polygonArrayxy.push(webMercatorUtils.xyToLngLat(polygonArray[i][0], polygonArray[i][1]));
            }

            // create polygon
            var polygon = {
                type: "polygon",
                rings: polygonArrayxy
            };

            // fill polygon with green color
            var simpleFillSymbol = {
                type: "simple-fill",
                color: [15, 166, 95, 0.5],  // orange, opacity 80%
                outline: {
                    color: [0, 0, 0],
                    width: 1
                }
            };

            // merge polygon and simplefillsymbol into polygongraphic
            var polygonGraphic = new Graphic({
                geometry: polygon,
                symbol: simpleFillSymbol,
            });

            // set attributes of the area
            polygonGraphic.attributes = {
                "date": item.date,
                "starttime": item.startTime,
                "endtime": item.endTime,
                "amount": item.amountOfGrass,
                "delivery": item.placeDelivery,
                "cost": item.cost
            };

            // set template of the area
            var template = {
                title: "Information",
                content: [
                    {
                        type: "fields",
                        fieldInfos: [
                            {
                                fieldName: "date",
                                label: "Date and time",
                            },
                            {
                                fieldName: "starttime",
                                label: "Start time"
                            },
                            {
                                fieldName: "endtime",
                                label: "End time"
                            },
                            {
                                fieldName: "delivery",
                                label: "Place of delivery"
                            },
                            {
                                fieldName: "cost",
                                label: "Costs of this place of mowed grass"
                            },
                            {
                                fieldName: "amount",
                                label: "Amount of grass mowed"
                            },
                        ]
                    }
                ],
            };

            // add polygon to the map
            polygonGraphic.popupTemplate = template;
            graphicsLayer.add(polygonGraphic);

        });
    });
});