package nl.bioinf.grass.database_utilities;

import nl.bioinf.grass.models.GrassInstanceCatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyAppDaoInMemory implements MyAppDao {
    private Map<String, GrassInstanceCatcher> grassDb = new HashMap<>();

//    public MyAppDaoInMemory() {
//        createDb();
//    }

    @Override
    public void connect() throws DatabaseException {
        //pass silently
    }

    @Override
    public List<GrassInstanceCatcher> getGrass() throws DatabaseException {
        //if (this.grassDb.containsKey(ID)) return this.grassDb.get(ID);
        return null;
    }

    @Override
    public void insertGrass(ArrayList<String> attri) throws DatabaseException {
        this.grassDb.put(attri.get(1), new GrassInstanceCatcher(attri.get(1), attri.get(2), attri.get(3),
                attri.get(4), attri.get(5), attri.get(6),
                attri.get(7), attri.get(8), attri.get(9)));
    }

    @Override
    public void disconnect() throws DatabaseException {
        //pass silently
    }

    private void createDb() {
        grassDb.put("5.5",
                new GrassInstanceCatcher("Johan", "De Boer", "01/06/2020", "09:40", "09:50",
                        "49", "school", "40", "7890"));
    }

}
