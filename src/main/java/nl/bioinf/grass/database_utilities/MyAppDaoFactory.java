package nl.bioinf.grass.database_utilities;

import nl.bioinf.grass.database_utilities.MyAppDao;
import nl.bioinf.grass.database_utilities.DbCredentials;
import nl.bioinf.grass.database_utilities.DbUser;

import java.io.IOException;

public class MyAppDaoFactory {

    private static MyAppDao daoInstance;

    /**
     * Code should be called at application startup
     */
    public static void initializeDataSource(String type) throws DatabaseException, ClassNotFoundException {
        if (daoInstance != null) {
            throw new IllegalStateException("DAO can be initialized only once");
        }
        switch (type) {
            case "dummy": {
                createDummyInstance();
                break;
            }
            case "mysql": {
                createMySQLInstance();
                break;
            }
            default:
                throw new IllegalArgumentException("unknown database type requested");
        }
    }

    /**
     * serves the dao instance
     *
     * @return
     */
    public static MyAppDao getDataSource() {
        if (daoInstance == null) {
            throw new IllegalStateException("DAO is not initialized; call initializeDataSource() first");
        }
        return daoInstance;
    }

    private static void createDummyInstance() throws DatabaseException, ClassNotFoundException {
        daoInstance = new MyAppDaoInMemory();
        daoInstance.connect();
    }

    private static void createMySQLInstance() throws DatabaseException {
        try {
            DbUser mySQLUser = DbCredentials.getMySQLUser();
            String dbUrl = "jdbc:mysql://" + mySQLUser.getHost() + "/" + mySQLUser.getDatabaseName();
            String dbUser = mySQLUser.getUserName();
            String dbPass = mySQLUser.getDatabasePassword();
            daoInstance = MyAppDaoMySQL.getInstance(dbUrl, dbUser, dbPass);
            daoInstance.connect();
        } catch (IOException | NoSuchFieldException | ClassNotFoundException e) {
            throw new DatabaseException(e);
        }
    }
}

