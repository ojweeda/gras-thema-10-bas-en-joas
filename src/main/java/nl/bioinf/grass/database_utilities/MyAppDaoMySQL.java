package nl.bioinf.grass.database_utilities;

import nl.bioinf.grass.models.GrassInstanceCatcher;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class MyAppDaoMySQL implements MyAppDao {
    private static final String GET_GRASS = "get_grass";
    private static final String INSERT_GRASS = "insert_grass";
    private static final String EDIT_GRASS = "edit_grass";
    private final String url;
    private final String dbUser;
    private final String dbPassword;
    private Connection connection;
    private Map<String, PreparedStatement> preparedStatements = new HashMap<>();

    /*singleton pattern*/
    private static MyAppDaoMySQL uniqueInstance;

    /**
     * singleton pattern
     * @param url
     * @param dbUser
     * @param dbPassword
     */
    private MyAppDaoMySQL(String url, String dbUser, String dbPassword) {
        this.url = url;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    /**
     * singleton pattern
     */
    public static MyAppDaoMySQL getInstance(String url, String dbUser, String dbPassword) {
        //lazy
        if (uniqueInstance == null) {
            uniqueInstance = new MyAppDaoMySQL(url, dbUser, dbPassword);
        }
        return uniqueInstance;
    }


    @Override
    public void connect() throws DatabaseException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, dbUser, dbPassword);
            prepareStatements();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());
        }
    }

    /**
     * prepares prepared statements for reuse
     * @throws SQLException
     */
    private void prepareStatements() throws SQLException {
        String fetchQuery = "SELECT * FROM Grass";
        PreparedStatement ps = connection.prepareStatement(fetchQuery);
        this.preparedStatements.put(GET_GRASS, ps);

        String insertQuery = "INSERT INTO Grass (FIRST_NAME, LAST_NAME, DATE, START_TIME, END_TIME, AMOUNT_GRASS, PLACE_DELIVERY, COST, AREA) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        ps = connection.prepareStatement(insertQuery);
        this.preparedStatements.put(INSERT_GRASS, ps);

        String editQuery = "INSERT INTO Grass (FIRST_NAME, LAST_NAME, DATE, START_TIME, END_TIME, AMOUNT_GRASS, PLACE_DELIVERY, COST, AREA) WHERE ID IS ?" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        ps = connection.prepareStatement(editQuery);
        this.preparedStatements.put(EDIT_GRASS, ps);
    }

    @Override
    public List<GrassInstanceCatcher> getGrass() throws DatabaseException  {
        List<GrassInstanceCatcher> grassInstances = new ArrayList<>();
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_GRASS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String first_name = rs.getString("FIRST_NAME");
                String last_name = rs.getString("LAST_NAME");
                String date = rs.getString("DATE");
                String start_time = rs.getString("START_TIME");
                String end_time = rs.getString("END_TIME");
                String amount_grass = rs.getString("AMOUNT_GRASS");
                String place_delivery = rs.getString("PLACE_DELIVERY");
                String cost = rs.getString("COST");
                String area = rs.getString("AREA");
                grassInstances.add(new GrassInstanceCatcher(first_name, last_name, date, start_time, end_time, amount_grass, place_delivery, cost, area));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());
        }
        return grassInstances;
    }

    @Override
    public void insertGrass(ArrayList<String> attributes) throws DatabaseException  {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_GRASS);
            ps.setString(1, attributes.get(0));
            ps.setString(2, attributes.get(1));
            ps.setString(3, attributes.get(2));
            ps.setString(4, attributes.get(3));
            ps.setString(5, attributes.get(4));
            ps.setString(6, attributes.get(5));
            ps.setString(7, attributes.get(6));
            ps.setString(8, attributes.get(7));
            ps.setString(9, attributes.get(8));
            ps.executeUpdate();
            //ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    @Override
    public void disconnect() throws DatabaseException {
        try{
            for( String key : this.preparedStatements.keySet() ){
                this.preparedStatements.get(key).close();
            }
        }catch( Exception e ){
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}

