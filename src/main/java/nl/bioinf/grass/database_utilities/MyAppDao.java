package nl.bioinf.grass.database_utilities;

import nl.bioinf.grass.models.GrassInstanceCatcher;

import java.util.List;

import java.util.ArrayList;

public interface MyAppDao{

    /**
     * connection logic should be put here
     * @throws DatabaseException
     */
    void connect() throws DatabaseException, ClassNotFoundException;

    /**
     * shutdown logic should be put here
     * @throws DatabaseException
     */
    void disconnect() throws DatabaseException;
    /**
     * fetches all grass instances.
     * @return List of Grass instances
     * @throws DatabaseException
     */
    List<GrassInstanceCatcher> getGrass() throws DatabaseException;

    /**
     * inserts a new Grass instance.
     * @throws DatabaseException
     */
    void insertGrass(ArrayList<String> attributes) throws DatabaseException;
}
