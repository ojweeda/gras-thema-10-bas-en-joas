package nl.bioinf.grass.database_utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

    public class DbCredentials {
        private static String configFileLocation;

        public static void setConfigFile(String fileName) {
            DbCredentials.configFileLocation = fileName;
        }

        public static String getMySQLdbPassword() throws PasswordRetrievalException, FileNotFoundException, IOException {
            DbUser u;

            try {
                u = getMySQLUser();
            } catch (NoSuchFieldException e) {
                throw new PasswordRetrievalException(e.getMessage());
            }

            return u.databasePassword;
        }

        /**
         *
         * @return @throws FileNotFoundException
         * @throws IOException
         * @throws NoSuchFieldException
         */
        public static DbUser getMySQLUser() throws FileNotFoundException, IOException, NoSuchFieldException {
            String passPropsFileName = System.getProperty("user.home") + File.separator + configFileLocation;
            Properties properties = new Properties();
            properties.load(new FileInputStream(passPropsFileName));

            DbUser u = new DbUser();
            if (!properties.containsKey("user")) {
                throw new NoSuchFieldException("field \"user\" is not found in the configuration");
            }
            u.userName = properties.getProperty("user");

            if (!properties.containsKey("password")) {
                throw new NoSuchFieldException("field \"password\" is not found in the configuration");
            }
            u.databasePassword = properties.getProperty("password");

            if (!properties.containsKey("host")) {
                throw new NoSuchFieldException("field \"host\" is not found in the configuration");
            }
            u.host = properties.getProperty("host");

            if (!properties.containsKey("database")) {
                throw new NoSuchFieldException("field \"database\" is not found in the configuration");
            }
            u.databaseName = properties.getProperty("database");

            return u;
        }

    }

