package nl.bioinf.grass.database_utilities;

public class PasswordRetrievalException extends Exception {

    public PasswordRetrievalException(String arg0) {
        super(arg0);
    }
}