package nl.bioinf.grass.servlets;

import nl.bioinf.grass.config.WebConfig;
import nl.bioinf.grass.database_utilities.DatabaseException;
import nl.bioinf.grass.database_utilities.MyAppDao;
import nl.bioinf.grass.database_utilities.MyAppDaoFactory;
import nl.bioinf.grass.models.GrassInstanceCatcher;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.thymeleaf.TemplateEngine;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet that serves the overview table page with information from the database
 */
@WebServlet(name = "OverviewTableServlet", urlPatterns = "/overview.table", loadOnStartup = 1)
public class OverviewTableServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private MyAppDao dataSource;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
        this.dataSource = MyAppDaoFactory.getDataSource();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("overview_table", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            WebConfig.configureResponse(response);
            WebContext ctx = new WebContext(
                    request,
                    response,
                    request.getServletContext(),
                    request.getLocale());

            // Get all data from database Grass
            List<GrassInstanceCatcher> grassInstances = dataSource.getGrass();

            // Add grass data as variable to be used in overview_table.html
            ctx.setVariable("grass_instances", grassInstances);
            templateEngine.process("overview_table", ctx, response.getWriter());
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
}
