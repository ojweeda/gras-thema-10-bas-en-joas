package nl.bioinf.grass.servlets;

import nl.bioinf.grass.config.WebConfig;
import nl.bioinf.grass.database_utilities.DatabaseException;
import nl.bioinf.grass.database_utilities.MyAppDao;
import nl.bioinf.grass.database_utilities.MyAppDaoFactory;
import nl.bioinf.grass.models.GrassInstanceCatcher;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.thymeleaf.TemplateEngine;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that serves the overview map
 */
@WebServlet(name = "OverviewMapServlet", urlPatterns = "/overview.map", loadOnStartup = 1)
public class OverviewMapServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private MyAppDao dataSource;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
        this.dataSource = MyAppDaoFactory.getDataSource();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("overview_map", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("overview_map", ctx, response.getWriter());
    }
}
