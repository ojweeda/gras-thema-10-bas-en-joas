package nl.bioinf.grass.servlets;

import nl.bioinf.grass.config.WebConfig;
import nl.bioinf.grass.database_utilities.DatabaseException;
import nl.bioinf.grass.database_utilities.MyAppDao;
import nl.bioinf.grass.database_utilities.MyAppDaoFactory;
import nl.bioinf.grass.models.GrassInstanceCatcher;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

/**
 * Servlet that provides the data for the application so it can be used in the HTML pages
 */
@WebServlet(name = "GrassDataServlet", urlPatterns = "/grassdata")
public class GrassDataServlet extends HttpServlet {
    private MyAppDao dataSource;

    @Override
    public void init() throws ServletException {
        this.dataSource = MyAppDaoFactory.getDataSource();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<GrassInstanceCatcher> grassList = new ArrayList<>();

        // Get all data from database and add it to the list
        try {
            grassList = dataSource.getGrass();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

        // create new json String
        String json = new Gson().toJson(grassList);

        // Return results as JSON
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);


    }
}
