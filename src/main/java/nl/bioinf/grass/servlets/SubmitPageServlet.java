package nl.bioinf.grass.servlets;

import nl.bioinf.grass.config.WebConfig;
import nl.bioinf.grass.database_utilities.DatabaseException;
import nl.bioinf.grass.database_utilities.MyAppDao;
import nl.bioinf.grass.database_utilities.MyAppDaoFactory;
import nl.bioinf.grass.models.InstanceGenerator;
import nl.bioinf.grass.models.GrassInstanceCatcher;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Servlet that serves the submit page,
 */
@WebServlet(name = "SubmitPageServlet", urlPatterns = "/submit.page")
@MultipartConfig
public class SubmitPageServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private MyAppDao dataSource;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
        this.dataSource = MyAppDaoFactory.getDataSource();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Instantiate class that will configure instances
        InstanceGenerator generator = new InstanceGenerator();

        // Get submitted values from request
        Map<String, String[]> data = request.getParameterMap();

        ArrayList<String> array = new ArrayList<>();
        ArrayList<String> coordinates = new ArrayList<>();

        // Loop through the submitted values
        for (Object item : data.values()) {
            String[] stringArray = (String[]) item;
            System.out.println(Arrays.toString(stringArray));
            int x_coord = 1;
            int y_coord = Arrays.toString(stringArray).length() - 1;
            array.add((Arrays.toString(stringArray)).substring(x_coord, y_coord));
        }
        // Make submitted values into Grass instance
        GrassInstanceCatcher instance1 = generator.createGrassInstance(array);

        // Print out submitted values
        System.out.println(instance1);
        System.out.println(array);

        // Add Grass instance to database
        try {
            dataSource.insertGrass(array);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("submit", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("submit", ctx, response.getWriter());
    }
}
