package nl.bioinf.grass.models;

import java.util.ArrayList;
import java.util.List;

/**
 * class that generates GrassInstanceCatcher object
 */
public class InstanceGenerator {

    public GrassInstanceCatcher createGrassInstance(ArrayList<String> attributes) {
        String firstName = attributes.get(0);
        String lastName = attributes.get(1);
        String date = attributes.get(2);
        String startTime = attributes.get(3);
        String endTime = attributes.get(4);
        String amountOfGrass = attributes.get(5);
        String placeDelivery = attributes.get(6);
        String cost = attributes.get(7);
        String area = attributes.get(8);
        return new GrassInstanceCatcher(firstName, lastName, date, startTime, endTime, amountOfGrass, placeDelivery, cost, area);
    }
}
