package nl.bioinf.grass.models;

/**
 * Class that provides getters and setters for all variables in the database
 */
public class GrassInstanceCatcher {
    private String firstName;
    private String lastName;
    private String date;
    private String startTime;
    private String endTime;
    private String amountOfGrass;
    private String placeDelivery;
    private String cost;
    private String area;


    public GrassInstanceCatcher(String firstName, String lastName, String date, String startTime, String endTime, String amountOfGrass, String placeDelivery, String cost, String area) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.amountOfGrass = amountOfGrass;
        this.placeDelivery = placeDelivery;
        this.cost = cost;
        this.area = area;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setAmountOfGrass(String amountOfGrass) {
        this.amountOfGrass = amountOfGrass;
    }

    public void setPlaceDelivery(String placeDelivery) {
        this.placeDelivery = placeDelivery;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setArea(String area) {
        this.area = area;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDate() {
        return date;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getAmountOfGrass() {
        return amountOfGrass;
    }

    public String getPlaceDelivery() {
        return placeDelivery;
    }

    public String getCost() {
        return cost;
    }

    public String getArea() {
        return area;
    }

    @Override
    public String toString() {
        return "Variables: " +
                "\nName: " + firstName + " " + lastName +
                "\nDate and time: " + date + ", " + startTime + " - " + endTime +
                "\nAmount of grass cut: " + amountOfGrass +
                "\nGrass is delivered at: " + placeDelivery +
                "\nTotal cost operation: " + cost +
                "\nTotal area: " + area +
                "\n";
    }
}
