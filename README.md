# Themeproject 10 Research Web Applications 
### Bas & Joas 

Assignments project for the course Introduction to programming with Java.
From September 2017 onwards, we will be using the IDE IntelliJ Idea. 
You will need a licence for this IDE, but we will provide one for you if you follow the
 bioinformatics curriculum.

### What is this README for?
* Brief summary
* Installation
* How to set up the project
* How to run the application

### Brief summary project
We collaborated with Staatsbosbeheer and Groeningen to make a web application where mowers can submit when, where and 
the amount they mowed. As well as the cost and the specific area they mowed, using an ArcGis map, which will then be 
added to a local database. We added two overview pages that give all the info to Staatsbosbeheer, the first is a map 
where all the areas are shown and you can click on any area to show more information about that specific spot. 
The other is a table with all the data from the database, where you can search for a date and get the data from that day.

### Installation
To execute this application the following programs are essential:
* Java 11.0.0
* Tomcat 9.0.27
* Thymeleaf 3.0.11
* MySQL database
* IntelliJ or other IDE of choice, which includes these:
    * HTML
    * CSS
    * JavaScript

### How to set up the project
* To set up this application, clone this repository to your pc.
* Open the project in your IDE
* To use a database, your own MySQL cnf file needs to be saved in your home directory.
* The filename of the cnf file needs to be filled into the web.xml file so the database can get the correct credentials.
    * between <param-value>OWN_FILENAME</param-value>
* To create the table which will contain the data you have to run the following script:
    * _nl/bioinf/grass/database_utilities/script.sql_
    * This will also insert 2 dummy instances into the database.
    
### How to run the application
* To run the application, there are some settings that are required.
    * Under _File/Settings/Build,Execution,Deployment/Deployment_ set the home directory for the TomCat server.
    * Add new Configuration, and point it to _http://localhost:8080/homepage_ 
    * If the new configuration warns you about no artifacts, click on _Fix_ and choose the first artifact (the one without exploded)
    * Click on run application and you're done!
* We recommend you use Chromium/Chrome to run the application



